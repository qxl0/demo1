package com.qiang.demo1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qiang.demo1.models.Session;

public interface SessionRepository extends JpaRepository<Session, Integer> {

}
