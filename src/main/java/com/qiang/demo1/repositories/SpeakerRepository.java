package com.qiang.demo1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qiang.demo1.models.Speaker;

public interface SpeakerRepository extends JpaRepository<Speaker, Integer> {

}
