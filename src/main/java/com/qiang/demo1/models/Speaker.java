package com.qiang.demo1.models;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "speakers")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Speaker {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer speaker_Id;
	
	private String first_name;
	private String last_name;
	private String title;
	private String company;
	private String speaker_bio;
	
	@Lob
	@Type(type="org.hibernate.type.BinaryType")
	private byte[] speaker_photo;
	
	@ManyToMany(mappedBy = "speakers")
	@JsonIgnore
	private List<Session> sessions;
	
	
	public Speaker() {
		
	}
	
	public byte[] getSpeaker_photo() {
		return this.speaker_photo;
	}
	public void setSpeaker_photo(byte[] value) {
		this.speaker_photo = value;
	}
	public List<Session> getSessions() {
		return this.sessions;
	}
	public void setSessions(List<Session> value) {
		this.sessions = value;
	}
	public Integer getSpeaker_id() {
		return this.speaker_Id;
	}
	public void setSpeaker_id(Integer value) {
		this.speaker_Id = value;
	}
	
	public String getLast_name() {
		return this.last_name;
	}
	public void setLast_name(String value) {
		this.last_name = value;
	}
	
	public String getFirst_name() {
		return this.first_name;
	}
	public void setFirst_name(String value) {
		this.first_name = value;
	}
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String value) {
		this.title = value;
	}
	public String getCompany() {
		return this.company;
	}
	public void setCompany(String value) {
		this.company = value;
	}
	
	public String getSpeaker_bio() {
		return this.speaker_bio;
	}
	public void setSpeaker_bio(String value) {
		this.speaker_bio = value;
	}
}
