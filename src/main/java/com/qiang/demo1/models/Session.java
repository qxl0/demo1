package com.qiang.demo1.models;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.JoinColumn;


@Entity(name = "sessions")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Session {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer session_id;
	private String session_name;
	private String session_description;
	private Integer session_length;
	
	@ManyToMany
	@JoinTable(
			name = "session_speakers",
			joinColumns = @JoinColumn(name = "session_id"),
			inverseJoinColumns = @JoinColumn(name = "speaker_id")
			)
		
	private List<Speaker> speakers;
	
	public Session() {
	}
	
	public List<Speaker> getSpeakers() {
		return this.speakers;
	}
	public void setSpeakers(List<Speaker> value) {
		this.speakers = value;
	}
	
	public Integer getSession_id() {
		return this.session_id;		
	}
	public void setSession_id(Integer value) {
		this.session_id = value;
	}
	public String getSession_name() {
		return this.session_name;
	}
	public void setSession_name(String value) {
		this.session_name = value;
	}
	public String getSession_description() {
		return this.session_description;
	}
	public void setSession_description(String value) {
		this.session_description =  value;
	}
	public Integer getSession_length() {
		return this.session_length;
	}
	public void setSession_length(Integer value) {
		this.session_length = value;
	}
}
